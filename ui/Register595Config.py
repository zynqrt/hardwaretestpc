from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from designer.Ui_Register595Config import Ui_Register595Config
from classes.Settings import Settings
from classes.Register import Register


class Register595Config(QGroupBox, Ui_Register595Config):
    status_changed = pyqtSignal(str)

    def __init__(self, device_address):
        super(Register595Config, self).__init__('Config')
        self._device_address = device_address
        self._base_address = 0
        self._offset = 0
        self._settings_full = None
        self._settings = None
        self._axi_frequency = 0
        self._updated = False

        self.setupUi(self)
        self.load_settings()
        self.spinDivider.setValue(self._settings['divider'])
        self.spinPhase.setValue(self._settings['phase'])
        self.chkReset.setChecked(self._settings['reset'])

        self.editRegAddress.setText('0x{:08X}'.format(self._base_address + self._offset))

        self.labelAxiFreqency.setText('IP frequency: {} Hz'.format(self._axi_frequency))
        self.deviderChanged(0)
        self.spinDivider.valueChanged.connect(self.deviderChanged)
        self.btnWrite.clicked.connect(self.send)

    def load_settings(self):
        self._settings_full = Settings().settings
        self._settings = self._settings_full['axi0']['reg595ip']['registers']['CR']
        self._base_address = int(self._settings_full['axi0']['reg595ip']['base_address'], base=16)
        self._offset = self._settings['offset']
        self._axi_frequency = self._settings_full['axi0']['frequency']

    def save_settings(self):
        self._settings['divider'] = self.spinDivider.value()
        self._settings['phase'] = self.spinPhase.value()
        self._settings['reset'] = self.chkReset.isChecked()
        s = Settings()
        s.save()

    def get_register_data(self):
        phase = self.spinPhase.value()
        devider = self.spinDivider.value()
        reset = int(self.chkReset.isChecked())
        return (reset << 8) | (devider << 4) | (phase << 0)

    def send(self):
        writer = Register()
        registers = [{'address': self._base_address + self._offset, 'data': self.get_register_data()}]
        ret = writer.write_one(self._device_address, registers[0])
        self.status_changed.emit('Reg595 Cfg: `{}`'.format(writer.last_error))
        if ret is False:
            return

        self.save_settings()

    def deviderChanged(self, value):
        sclk_frequency = self._axi_frequency / (2 * (value + 1))
        self.labelMcClock.setText('-> {:2.3f} MHz'.format(sclk_frequency/1e6))

    def read_from_device(self):
        pass

    def device_address_changed(self, device_address):
        self._device_address = device_address
