from PIL import Image
from PIL.ImageQt import ImageQt
import struct
import numpy as np

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtGui import QPixmap

from designer import Ui_RegisterAD9826
from classes import Settings
from classes.Register import Register
from classes.CISReader import CISReader
from classes.ImageProcessor import ImageProcessor
from math import log10
import logging
import pyzbar.pyzbar


class RegisterAD9826(QGroupBox, Ui_RegisterAD9826.Ui_RegisterAD9826):
    status_changed = pyqtSignal(str)

    def __init__(self, device_address):
        super(RegisterAD9826, self).__init__()
        self.logger = logging.getLogger('CIS')
        self.logger.setLevel(logging.DEBUG)

        self._resolutions = {}
        self._device_address = device_address
        self._updated = False
        self._setting_full = None
        self._settings_config = None
        self._settings_mux = None
        self._settings_red_pga = None
        self._settings_green_pga = None
        self._settings_blue_pga = None
        self._settings_red_offset = None
        self._settings_green_offset = None
        self._settings_blue_offset = None

        self._received_image = None

        self.setupUi(self)
        self.load_settings()
        self.load_settings_to_ui()

        self.sliders_update(0)

        # Подключаем все сигналы изменения состояния полей ввода на вывод звездочки на кнопку Write
        self.sliderRedPGA.valueChanged.connect(self.settings_updated)
        self.sliderGreenPGA.valueChanged.connect(self.settings_updated)
        self.sliderBluePGA.valueChanged.connect(self.settings_updated)
        self.sliderRedOffset.valueChanged.connect(self.settings_updated)
        self.sliderGreenOffset.valueChanged.connect(self.settings_updated)
        self.sliderBlueOffset.valueChanged.connect(self.settings_updated)
        self.cmbInputRange.currentIndexChanged.connect(self.settings_updated)
        self.cmbRefence.currentIndexChanged.connect(self.settings_updated)
        self.cmb3ChMode.currentIndexChanged.connect(self.settings_updated)
        self.cmbCDSMode.currentIndexChanged.connect(self.settings_updated)
        self.cmbClamp.currentIndexChanged.connect(self.settings_updated)
        self.cmbPowerDown.currentIndexChanged.connect(self.settings_updated)
        self.cmbOneByteOut.currentIndexChanged.connect(self.settings_updated)
        self.cmbMux.currentIndexChanged.connect(self.settings_updated)
        self.chkMuxRed.stateChanged.connect(self.settings_updated)
        self.chkMuxGreen.stateChanged.connect(self.settings_updated)
        self.chkMuxBlue.stateChanged.connect(self.settings_updated)

        self.sliderRedPGA.valueChanged.connect(self.sliders_update)
        self.sliderGreenPGA.valueChanged.connect(self.sliders_update)
        self.sliderBluePGA.valueChanged.connect(self.sliders_update)
        self.sliderRedOffset.valueChanged.connect(self.sliders_update)
        self.sliderGreenOffset.valueChanged.connect(self.sliders_update)
        self.sliderBlueOffset.valueChanged.connect(self.sliders_update)

        self.btnWrite.clicked.connect(self.send)
        self.btnReadImage.clicked.connect(self.read_image)

    def load_settings(self):
        s = Settings.Settings()
        self._setting_full = s.settings
        self._settings_config = self._setting_full['ad9826']['registers']['configuration']
        self._settings_mux = self._setting_full['ad9826']['registers']['mux_config']
        self._settings_red_pga = self._setting_full['ad9826']['registers']['red_pga']
        self._settings_green_pga = self._setting_full['ad9826']['registers']['green_pga']
        self._settings_blue_pga = self._setting_full['ad9826']['registers']['blue_pga']
        self._settings_red_offset = self._setting_full['ad9826']['registers']['red_offset']
        self._settings_green_offset = self._setting_full['ad9826']['registers']['green_offset']
        self._settings_blue_offset = self._setting_full['ad9826']['registers']['blue_offset']

        resolutions = self._setting_full['cis']['resolutions']
        for i in range(len(resolutions)):
            self._resolutions[i] = (resolutions[i]['dpi'],
                                    resolutions[i]['pixels_inactive'], resolutions[i]['pixels_active'],
                                    resolutions[i]['pixels_inactive'] + resolutions[i]['pixels_active'])

    def save_settings(self):
        self._settings_config['input_range'] = self.cmbInputRange.currentIndex()
        self._settings_config['vref'] = self.cmbRefence.currentIndex()
        self._settings_config['3ch_mode'] = self.cmb3ChMode.currentIndex()
        self._settings_config['cds_on'] = self.cmbCDSMode.currentIndex()
        self._settings_config['clamp'] = self.cmbClamp.currentIndex()
        self._settings_config['pwr_down'] = self.cmbPowerDown.currentIndex()
        self._settings_config['1_byte_out'] = self.cmbOneByteOut.currentIndex()

        self._settings_mux['rgb_bgr'] = self.cmbMux.currentIndex()
        self._settings_mux['red'] = int(self.chkMuxRed.isChecked())
        self._settings_mux['green'] = int(self.chkMuxGreen.isChecked())
        self._settings_mux['blue'] = int(self.chkMuxBlue.isChecked())

        self._settings_red_pga['value'] = self.sliderRedPGA.value()
        self._settings_green_pga['value'] = self.sliderGreenPGA.value()
        self._settings_blue_pga['value'] = self.sliderBluePGA.value()
        self._settings_red_offset['value'] = self.sliderRedOffset.value()
        self._settings_green_offset['value'] = self.sliderGreenOffset.value()
        self._settings_blue_offset['value'] = self.sliderBlueOffset.value()
        self._setting_full['cis']['brightness'] = self.spinBrightness.value()

        s = Settings.Settings()
        s.save()

    def load_settings_to_ui(self):
        self.cmbInputRange.setCurrentIndex(self._settings_config['input_range'])
        self.cmbRefence.setCurrentIndex(self._settings_config['vref'])
        self.cmb3ChMode.setCurrentIndex(self._settings_config['3ch_mode'])
        self.cmbCDSMode.setCurrentIndex(self._settings_config['cds_on'])
        self.cmbClamp.setCurrentIndex(self._settings_config['clamp'])
        self.cmbPowerDown.setCurrentIndex(self._settings_config['pwr_down'])
        self.cmbOneByteOut.setCurrentIndex(self._settings_config['1_byte_out'])

        self.cmbMux.setCurrentIndex(self._settings_mux['rgb_bgr'])
        self.chkMuxRed.setChecked(self._settings_mux['red'])
        self.chkMuxGreen.setChecked(self._settings_mux['green'])
        self.chkMuxBlue.setChecked(self._settings_mux['blue'])

        self.sliderRedPGA.setValue(self._settings_red_pga['value'])
        self.sliderGreenPGA.setValue(self._settings_green_pga['value'])
        self.sliderBluePGA.setValue(self._settings_blue_pga['value'])
        self.sliderRedOffset.setValue(self._settings_red_offset['value'])
        self.sliderGreenOffset.setValue(self._settings_green_offset['value'])
        self.sliderBlueOffset.setValue(self._settings_blue_offset['value'])
        self.spinBrightness.setValue(self._setting_full['cis']['brightness'])

        for resolution in self._resolutions:
            self.cmbResolution.addItem('{} dpi'.format(self._resolutions[resolution][0]))

    def settings_updated(self):
        if not self._updated:
            self.btnWrite.setText(self.btnWrite.text() + '*')
            self._updated = True

    def send(self):
        regs = [self.get_register_config(),
                self.get_register_mux(),
                {'address': int(self._settings_red_pga['address'], base=2), 'data': self.sliderRedPGA.value()},
                {'address': int(self._settings_green_pga['address'], base=2), 'data': self.sliderGreenPGA.value()},
                {'address': int(self._settings_blue_pga['address'], base=2), 'data': self.sliderBluePGA.value()},
                {'address': int(self._settings_red_offset['address'], base=2),
                 'data': self.calculate_offset_reg(self.sliderRedOffset.value())},
                {'address': int(self._settings_green_offset['address'], base=2),
                 'data': self.calculate_offset_reg(self.sliderGreenOffset.value())},
                {'address': int(self._settings_blue_offset['address'], base=2),
                 'data': self.calculate_offset_reg(self.sliderBlueOffset.value())}]

        writer = Register(wr_command='wrcis')
        ret = writer.write_many(self._device_address, regs)
        self.status_changed.emit('CIS AD9826: `{}`'.format(writer.last_error))
        if ret is False:
            return

        if self._updated:
            self.save_settings()
            self.btnWrite.setText(self.btnWrite.text()[:-1])
            self._updated = False

    def get_register_config(self):
        d7 = self.cmbInputRange.currentIndex()
        d6 = self.cmbRefence.currentIndex()
        d5 = self.cmb3ChMode.currentIndex()
        d4 = self.cmbCDSMode.currentIndex()
        d3 = self.cmbClamp.currentIndex()
        d2 = self.cmbPowerDown.currentIndex()
        d0 = self.cmbOneByteOut.currentIndex()
        reg = (d7 << 7) | (d6 << 6) | (d5 << 5) | (d4 << 4) | (d3 << 3) | (d2 << 2) | (d0 << 0)
        address = int(self._settings_config['address'], base=2)
        self._settings_config['value'] = '0b{:08b}'.format(reg)
        return {'address': address, 'data': reg}

    def get_register_mux(self):
        d7 = self.cmbMux.currentIndex()
        d6 = int(self.chkMuxRed.isChecked())
        d5 = int(self.chkMuxGreen.isChecked())
        d4 = int(self.chkMuxBlue.isChecked())
        reg = (d7 << 7) | (d6 << 6) | (d5 << 5) | (d4 << 4)
        address = int(self._settings_mux['address'], base=2)
        self._settings_mux['value'] = '0b{:08b}'.format(reg)
        return {'address': address, 'data': reg}

    def calculate_pga(self, g):
        gain_vv = 6.0 / (1.0 + 5.0*(63-g)/63)
        gain_db = 20 * log10(gain_vv)
        return gain_db, gain_vv

    def _get_pga_string(self, gain):
        gain_db, gain_vv = self.calculate_pga(gain)
        return '{:3d}, {:>4.2f} dB ({:>4.3} V/V)'.format(gain, gain_db, gain_vv)

    def calculate_offset(self, offset):
        g = 300/255
        if offset == 0:
            return 0.0
        no = offset * g
        return no

    def _get_offset_string(self, offset):
        r = '0b{:09b}'.format(self.calculate_offset_reg(offset))
        return '{}, {:> 7.2f} mV'.format(r, self.calculate_offset(offset))

    def calculate_offset_reg(self, offset):
        sign = 0b1_0000_0000 if offset < 0 else 0b0_0000_0000
        reg = sign | abs(offset)
        return reg

    def sliders_update(self, value):
        self.labelRedPGA.setText(self._get_pga_string(self.sliderRedPGA.value()))
        self.labelGreenPGA.setText(self._get_pga_string(self.sliderGreenPGA.value()))
        self.labelBluePGA.setText(self._get_pga_string(self.sliderBluePGA.value()))

        self.labelRedOffset.setText(self._get_offset_string(self.sliderRedOffset.value()))
        self.labelGreenOffset.setText(self._get_offset_string(self.sliderGreenOffset.value()))
        self.labelBlueOffset.setText(self._get_offset_string(self.sliderBlueOffset.value()))

    def device_address_changed(self, device_address):
        self._device_address = device_address

    def read_image(self):
        if self.radioColorR.isChecked():
            color = 'R'
        elif self.radioColorG.isChecked():
            color = 'G'
        elif self.radioColorB.isChecked():
            color = 'B'
        else:
            color = 'X'

        resolution = self._resolutions[self.cmbResolution.currentIndex()]
        reader = CISReader()
        image_data = reader.get_image(self._device_address, self.spinLines.value(), color, self.spinBrightness.value(), resolution)
        if image_data is not None:
            self.process_image_data(image_data)

        self.status_changed.emit(reader.last_error)

    def process_image_data(self, image_raw):
        if len(image_raw) == 0:
            return
        with open('tmp/raw_bytes.d', 'wb') as f:
            f.write(image_raw)

        imp = ImageProcessor(self._resolutions[self.cmbResolution.currentIndex()][0])
        im_r, im_g = imp.bytes_to_pil(image_raw, self.spinLines.value())
        im_r.save('tmp/red.png')
        im_g.save('tmp/green.png')

        self.logger.debug('Received {} bytes for image'.format(len(image_raw)))
        self.labelImageR.setFixedHeight(80)
        self.labelImageR.setFixedWidth(660)
        self.labelImageG.setFixedHeight(80)
        self.labelImageG.setFixedWidth(660)

        decode_r = pyzbar.pyzbar.decode(im_r)
        decode_g = pyzbar.pyzbar.decode(im_g)

        label_size = (self.labelImageR.width(), self.labelImageR.height())
        im_r = im_r.resize(label_size)
        received_image_r = QPixmap.fromImage(ImageQt(im_r)).copy()
        self.labelImageR.setPixmap(received_image_r)

        label_size = (self.labelImageG.width(), self.labelImageG.height())
        im_g = im_g.resize(label_size)
        received_image_g = QPixmap.fromImage(ImageQt(im_g)).copy()
        self.labelImageG.setPixmap(received_image_g)

        if len(decode_r) > 0:
            decoded_r_str = 'Decoded R channel. Type: {}, data: {}'.format(decode_r[0][1], decode_r[0][0])
            self.logger.info('Decoded R channel: {}'.format(decode_r))
        else:
            decoded_r_str = 'Decoded R channel: None'
        self.labelResultR.setText(decoded_r_str)

        if len(decode_g) > 0:
            decoded_g_str = 'Decoded G channel. Type: {}, data: {}'.format(decode_g[0][1], decode_g[0][0])
            self.logger.info('Decoded G channel: {}'.format(decode_g))
        else:
            decoded_g_str = 'Decoded G channel: None'
        self.labelResultG.setText(decoded_g_str)
