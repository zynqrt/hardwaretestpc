from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from designer.Ui_Register595Leds import Ui_Register595Leds
from classes.Settings import Settings
from classes.Register import Register


_red_style = '''
QCheckBox::indicator::checked {
    background-color: rgba(255, 0, 0, 255);
    border: 2px solid black;
    width: 20px;
    height: 20px;
}

QCheckBox::indicator::unchecked {
    background-color: rgba(255, 0, 0, 0);
    border: 2px solid red;
    width: 20px;
    height: 20px;
}
'''

_green_style = '''
QCheckBox::indicator::checked {
    background-color: rgba(0, 255, 0, 255);
    border: 2px solid black;
    width: 20px;
    height: 20px;
}

QCheckBox::indicator::unchecked {
    background-color: rgba(0, 255, 0, 0);
    border: 2px solid green;
    width: 20px;
    height: 20px;
}
'''


class Register595Leds(QGroupBox, Ui_Register595Leds):
    status_changed = pyqtSignal(str)

    def __init__(self, device_address):
        super(Register595Leds, self).__init__('LEDs')
        self._device_address = device_address
        self._base_address = 0
        self._offset = 0
        self._settings_full = None
        self._settings = None
        self._updated = False
        self.setupUi(self)
        self.load_settings()

        # validator = QRegExpValidator(QRegExp('0x[0-9A-Fa-f]{1,8}'))
        # self.editRegAddress.setValidator(validator)
        self.editRegAddress.setText('0x{:08X}'.format(self._base_address + self._offset))

        self.chkRed0.setStyleSheet(_red_style)
        self.chkRed1.setStyleSheet(_red_style)
        self.chkRed2.setStyleSheet(_red_style)
        self.chkRed3.setStyleSheet(_red_style)
        self.chkGreen0.setStyleSheet(_green_style)
        self.chkGreen1.setStyleSheet(_green_style)
        self.chkGreen2.setStyleSheet(_green_style)
        self.chkGreen3.setStyleSheet(_green_style)
        self.reds = [self.chkRed0, self.chkRed1, self.chkRed2, self.chkRed3]
        self.greens = [self.chkGreen0, self.chkGreen1, self.chkGreen2, self.chkGreen3]
        self.leds_all = self.greens[::-1] + self.reds[::-1]

        for led in self.leds_all:
            led.stateChanged.connect(self.checkboxChanged)

        self.btnWrite.clicked.connect(self.send)
        self.btnRead.clicked.connect(self.read_from_device)

    def load_settings(self):
        s = Settings()
        self._settings_full = s.settings
        self._settings = self._settings_full['axi0']['reg595ip']['registers']['LED']
        self._base_address = int(self._settings_full['axi0']['reg595ip']['base_address'], base=16)
        self._offset = self._settings['offset']

    def update_leds(self, status):
        index = 0
        for led in self.leds_all:
            ss = led.blockSignals(True)
            if status & (1 << index):
                led.setChecked(True)
            else:
                led.setChecked(False)
            led.blockSignals(ss)
            index += 1

    def get_register_data(self):
        reg_data = 0
        index = 0
        for led in self.leds_all:
            st = int(led.isChecked())
            reg_data |= (st << index)
            index += 1

        return reg_data

    def checkboxChanged(self):
        self.send()

    def send(self):
        writer = Register()
        register = {'address': self._base_address + self._offset, 'data': self.get_register_data()}
        ret = writer.write_one(self._device_address, register)
        self.status_changed.emit('LEDS: `{}`'.format(writer.last_error))

        if ret is False:
            return

        if self._updated:
            self.btnWrite.setText(self.btnWrite.text()[:-1])
            self._updated = False

    def read_from_device(self):
        reader = Register()
        ret, data = reader.read_one(self._device_address, {'address': self._base_address + self._offset})
        self.status_changed.emit(reader.last_error)
        if ret:
            self.update_leds(data)

    def device_address_changed(self, device_address):
        self._device_address = device_address
