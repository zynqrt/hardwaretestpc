from designer import Ui_MainWindow
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QSpacerItem, QSizePolicy
from . import Stepper
from . import Register595Leds
from . import Register595Config
from . import RegisterAD9826
from . import RegisterCIS
from . import StepperNg

from classes.Settings import Settings
import logging


class MainWindow(QMainWindow, Ui_MainWindow.Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.logger = logging.getLogger('Main')
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug('Start Hardware Test Program')
        self._settings_full = Settings().settings

        for i in range(4):
            stepper = Stepper.Stepper(i, 'Stepper {}'.format(i+1), self.editAddressIP.text())
            vbox = QVBoxLayout()
            vbox.addWidget(stepper)
            spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
            vbox.addItem(spacer)
            self.horizontalLayoutMotors.addLayout(vbox)
            self.btnReadFromDevice.clicked.connect(stepper.read_from_device)
            self.editAddressIP.textChanged.connect(stepper.device_address_changed)
            stepper.status_changed.connect(self.status_changed)

        reg595config = Register595Config.Register595Config(self.editAddressIP.text())
        self.horizontalLayoutConfig.addWidget(reg595config)
        reg595config.status_changed.connect(self.status_changed)
        self.editAddressIP.textChanged.connect(reg595config.device_address_changed)

        leds = Register595Leds.Register595Leds(self.editAddressIP.text())
        leds.status_changed.connect(self.status_changed)
        self.editAddressIP.textChanged.connect(leds.device_address_changed)
        self.horizontalLayoutConfig.addWidget(leds)

        spacer = QSpacerItem(40, 20, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)
        self.horizontalLayoutConfig.addItem(spacer)

        regAD9826 = RegisterAD9826.RegisterAD9826(self.editAddressIP.text())
        self.verticalLayoutCIS.addWidget(regAD9826)
        self.editAddressIP.textChanged.connect(regAD9826.device_address_changed)
        regAD9826.status_changed.connect(self.status_changed)

        regCIS = RegisterCIS.RegisterCIS(self.editAddressIP.text())
        self.verticalLayoutCISRegs.addWidget(regCIS)
        self.editAddressIP.textChanged.connect(regCIS.device_address_changed)
        regCIS.status_changed.connect(self.status_changed)

        stepperNg = StepperNg.StepperNg(self.editAddressIP.text())
        self.verticalLayoutStepperNg.addWidget(stepperNg)
        self.editAddressIP.textChanged.connect(stepperNg.device_address_changed)
        stepperNg.status_changed.connect(self.status_changed)

        self.btnReadFromDevice.clicked.connect(reg595config.read_from_device)
        self.btnReadFromDevice.clicked.connect(leds.read_from_device)

        self.editAddressIP.setText(self._settings_full['device_address'])
        self.editAddressIP.textChanged.connect(self.ip_changed)

    def ip_changed(self, ipaddress):
        s = Settings()
        s.settings['device_address'] = ipaddress

    def status_changed(self, status):
        self.statusbar.showMessage(status)
