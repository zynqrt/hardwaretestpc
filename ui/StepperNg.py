import socket
import select
from PyQt5.QtCore import QRegExp, pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtGui import QRegExpValidator
from designer import Ui_StepperNg
from classes import Settings, Register
import logging


class StepperNg(QGroupBox, Ui_StepperNg.Ui_StepperNg):
    status_changed = pyqtSignal(str)

    def __init__(self, device_address):
        super(StepperNg, self).__init__()
        self.logger = logging.getLogger('StepperNg')
        self.logger.setLevel(logging.DEBUG)

        self._device_address = device_address
        self._settings_full = None
        self._settings = None
        self._last_error = ''
        self._frequency = 0.0

        self.setupUi(self)
        self.load_settings()

        valid_int = '[0-9]*'
        self.editAccTime.setValidator(QRegExpValidator(QRegExp(valid_int)))
        self.editDecTime.setValidator(QRegExpValidator(QRegExp(valid_int)))
        self.editConstSpeedTime.setValidator(QRegExpValidator(QRegExp(valid_int)))

        self.btnWrite.clicked.connect(self.send_config)
        self.btnStart.clicked.connect(self.send_start)
        self.btnStop.clicked.connect(self.send_stop)
        self.btnHumanToReg.clicked.connect(self.to_reg)
        self.btnRegToHuman.clicked.connect(self.to_human)

        self.spinDecTime_ms.valueChanged.connect(self.updated_time)
        self.spinAccTime_ms.valueChanged.connect(self.updated_time)
        self.spinConstTime_ms.valueChanged.connect(self.updated_time)

    def updated_time(self):
        t_all = self.spinAccTime_ms.value() + self.spinConstTime_ms.value() + self.spinDecTime_ms.value()
        self.editAllTime.setText('Время движения: {:.3f} ms'.format(t_all))

    def to_human(self):
        dt = 1 / self._frequency * 1e3  # значения времени в мс
        acc_time_ms = dt * int(self.editAccTime.text())
        const_time_ms = dt * int(self.editConstSpeedTime.text())
        dec_time_ms = dt * int(self.editDecTime.text())
        self.spinAccTime_ms.setValue(acc_time_ms)
        self.spinConstTime_ms.setValue(const_time_ms)
        self.spinDecTime_ms.setValue(dec_time_ms)

    def to_reg(self):
        dt = 1 / self._frequency * 1e3  # значения времени в мс
        acc_time = round(self.spinAccTime_ms.value() / dt)
        const_time = round(self.spinConstTime_ms.value() / dt)
        dec_time = round(self.spinDecTime_ms.value() / dt)
        self.editAccTime.setText(str(acc_time))
        self.editConstSpeedTime.setText(str(const_time))
        self.editDecTime.setText(str(dec_time))

        acceleration = self.spinAcc.value()
        deceleration = self.spinDec.value()
        step_bit = self.spinSelection.value()

        v_acc_end = (acc_time + 1) * acceleration
        s_acc_end = acc_time * (acc_time + 1) // 2 * acceleration
        s_all = s_acc_end
        s_const_end = v_acc_end * (const_time + 1)
        s_all += s_const_end
        v_dec_end = v_acc_end - (dec_time + 1) * deceleration
        s_dec_end = dec_time * (dec_time + 1) // 2 * deceleration
        s_all += s_dec_end
        # step_count = s_all >> (step_bit + 1)
        step_count = self.get_steps(s_all, step_bit)

        self.editSpeedAccEnd.setText(str(v_acc_end))
        self.editPositionAccEnd.setText(str(s_acc_end))
        self.editPositionConstEnd.setText(str(s_const_end))
        self.editSpeedDecEnd.setText(str(v_dec_end))
        self.editPositionEnd.setText(str(s_all))
        self.editSteps.setText(str(step_count))
        step_freq = self.calc_freq(acceleration, acc_time, step_bit, self._frequency)
        self.editStepFrequency.setText('{} Hz'.format(step_freq))

    def calc_freq(self, acceleration, acceleration_time, step_bit, frequency):
        v_acc = (acceleration_time + 1) * acceleration
        s_acc_end = acceleration_time * (acceleration_time + 1) // 2 * acceleration
        steps_start = self.get_steps(s_acc_end, step_bit)
        dt = 1 / frequency
        s_const_end = s_acc_end + v_acc * round(1.0 / dt)  # Двигаемся 1 секунду виртуально
        steps_end = self.get_steps(s_const_end, step_bit)
        delta_steps = steps_end - steps_start
        return delta_steps


    def get_steps(self, position, step_bit):
        position_reg_width = self._settings['position_reg_width']
        step_reg_width = self._settings['step_reg_width']
        step_reg = position >> (position_reg_width - step_reg_width)
        step_count = step_reg >> (step_bit - (position_reg_width - step_reg_width) + 1)
        return step_count

    def send_config(self):
        # "stepper_cfg <acc> <acc_time> <constspeed_time> <dec> <dec_t> <sel> <dir>
        request = 'stepper_cfg {acc:} {acc_time:} {constspeed_time:} {dec:} {dec_time} {sel} {dir}\r\n'.format(
            acc=self.spinAcc.value(), acc_time=self.editAccTime.text(), constspeed_time=self.editConstSpeedTime.text(),
            dec=self.spinDec.value(), dec_time=self.editDecTime.text(), sel=self.spinSelection.value(),
            dir=1 if self.chkDirection.isChecked() else 0
        )
        self.logger.debug('Send config: {}'.format(request[:-2]))
        try:
            sock = self.sock_open(self._device_address)
            bdata = request.encode('utf-8')
            sock.send(bdata)
            status = b''
            r = self.sock_read(sock, timeout=0.2, size=4096)
            while r != b'':
                status += r
                r = self.sock_read(sock, timeout=0.2, size=4096)

            self._last_error = '{}: `{}` -> {}'.format(self._device_address, request[:-2], status[:-2].decode('ascii'))
            self.logger.info(self._last_error)
            self.status_changed.emit(self._last_error)
            sock.close()

        except Exception as e:
            self._last_error = '{}: Socket write error, error {}'.format(self._device_address, e)
            self.logger.error(self._last_error)

        self.save_settings()

    def send_start_stop(self, cmd):
        self.logger.debug('Send {}'.format(cmd))
        cmd += '\r\n'
        request = cmd.encode('utf-8')
        try:
            sock = self.sock_open(self._device_address)
            sock.send(request)
            status = self.sock_read(sock, size=64)
            status += self.sock_read(sock, size=64)
            self._last_error = '{}: `{}` -> {}'.format(self._device_address, cmd[:-2], status[:-2].decode('ascii'))
            self.status_changed.emit(self._last_error)
            self.logger.info(self._last_error)
            sock.close()

        except Exception as e:
            self._last_error = '{}: Socket write error, error {}'.format(self._device_address, e)
            self.logger.error(self._last_error)
            self.status_changed.emit(self._last_error)

    def send_start(self):
        self.send_start_stop('stepper_start')

    def send_stop(self):
        self.send_start_stop('stepper_stop')

    def load_settings(self):
        s = Settings.Settings()
        self._settings_full = s.settings
        self._settings = self._settings_full['stepperng']
        self._frequency = self._settings['frequency']

        self.labelFrequency.setText('Частота модуля: {:.1f} MHz'.format(self._frequency/1e6))
        self.spinAcc.setValue(self._settings['acceleration'])
        self.spinAccTime_ms.setValue(self._settings['acceleration_time'])
        self.spinConstTime_ms.setValue(self._settings['constantspeed_time'])
        self.spinDec.setValue(self._settings['deceleration'])
        self.spinDecTime_ms.setValue(self._settings['deceleration_time'])
        self.spinSelection.setValue(self._settings['select'])
        self.to_reg()
        self.updated_time()

    def save_settings(self):
        self._settings['acceleration'] = self.spinAcc.value()
        self._settings['acceleration_time'] = self.spinAccTime_ms.value()
        self._settings['constantspeed_time'] = self.spinConstTime_ms.value()
        self._settings['deceleration'] = self.spinDec.value()
        self._settings['deceleration_time'] = self.spinDecTime_ms.value()
        self._settings['select'] = self.spinSelection.value()
        s = Settings.Settings()
        s.save()

    def device_address_changed(self, device_address):
        self._device_address = device_address

    def sock_open(self, device_address):
        sock = socket.socket()
        sock.settimeout(1.0)
        sock.connect((device_address, 8085))
        sock.setblocking(False)
        self.sock_read(sock)
        return sock

    def sock_read(self, sock, timeout=0.1, size=4096):
        ready = select.select([sock], [], [], timeout)
        if ready[0]:
            data = sock.recv(size)
        else:
            data = b''
        return data

