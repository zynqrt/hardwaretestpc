from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from designer import Ui_RegisterCIS
from classes import Settings, Register
import logging


class RegisterCIS(QGroupBox, Ui_RegisterCIS.Ui_RegisterCIS):
    status_changed = pyqtSignal(str)

    def __init__(self, device_address):
        super(RegisterCIS, self).__init__()
        self.logger = logging.getLogger('RegCIS')
        self.logger.setLevel(logging.DEBUG)

        self._device_address = device_address
        self._base_address = 0
        self._offset = 0
        self._settings_full = None
        self._setting = None

        self.setupUi(self)
        self.load_settings()

        self.btnCR0Send.clicked.connect(self.send_cr0)
        self.btnCR1Send.clicked.connect(self.send_cr1)
        self.btnCR2Send.clicked.connect(self.send_cr2)
        self.btnCR3Send.clicked.connect(self.send_cr3)

    def send(self, address, data):
        writer = Register.Register()
        register = {'address': address, 'data': data}
        writer.write_one(self._device_address, register)
        self.status_changed.emit('CIS register: {}'.format(writer.last_error))
        self.logger.debug('Write Reg: {}'.format(writer.last_error))
        self.save_settings()

    def send_cr0(self):
        address = self._base_address + self._setting['CR0']['offset']
        data = self.get_register_cr0()
        self.send(address, data)

    def send_cr1(self):
        address = self._base_address + self._setting['CR1']['offset']
        data = self.get_register_cr1()
        self.send(address, data)

    def send_cr2(self):
        address = self._base_address + self._setting['CR2']['offset']
        data = self.get_register_cr2()
        self.send(address, data)

    def send_cr3(self):
        address = self._base_address + self._setting['CR3']['offset']
        data = self.get_register_cr3()
        self.send(address, data)

    def load_settings(self):
        s = Settings.Settings()
        self._settings_full = s.settings
        self._setting = self._settings_full['axi0']['regCISip']['registers']
        self._base_address = int(self._settings_full['axi0']['regCISip']['base_address'], base=16)

        self.spinCR0Fall.setValue(self._setting['CR0']['FALL'])
        self.spinCR0Rise.setValue(self._setting['CR0']['RISE'])
        self.spinCR0Sample0.setValue(self._setting['CR0']['SAMPLE0'])
        self.spinCR0Sample1.setValue(self._setting['CR0']['SAMPLE1'])
        self.chkCR0PRBS.setChecked(self._setting['CR0']['PRBS'])
        self.chkCR0Enable.setChecked(self._setting['CR0']['ENABLE'])

        self.spinCR1Phase.setValue(self._setting['CR1']['PHASE'])
        self.spinCR1Width.setValue(self._setting['CR1']['WIDTH'])
        self.chkCR1Enable.setChecked(self._setting['CR1']['ENABLE'])
        self.chkCR1Skip.setChecked(True if self._setting['CR1']['SKIP'] == 1 else False)

        self.spinCR2Phase.setValue(self._setting['CR2']['PHASE'])
        self.spinCR2Width.setValue(self._setting['CR2']['WIDTH'])
        self.chkCR2Enable.setChecked(self._setting['CR2']['ENABLE'])
        self.chkCR2Skip.setChecked(True if self._setting['CR2']['SKIP'] == 1 else False)

        self.spinCR3SiPhase.setValue(self._setting['CR3']['SI_PHASE'])
        self.spinCR3ClkPhase.setValue(self._setting['CR3']['CLK_PHASE'])
        self.spinCR3SiWidth.setValue(self._setting['CR3']['SI_WIDTH'])

    def save_settings(self):
        self._setting['CR0']['FALL'] = self.spinCR0Fall.value()
        self._setting['CR0']['RISE'] = self.spinCR0Rise.value()
        self._setting['CR0']['SAMPLE0'] = self.spinCR0Sample0.value()
        self._setting['CR0']['SAMPLE1'] = self.spinCR0Sample1.value()
        self._setting['CR0']['PRBS'] = True if self.chkCR0PRBS.isChecked() else False
        self._setting['CR0']['ENABLE'] = True if self.chkCR0Enable.isChecked() else False

        self._setting['CR1']['PHASE'] = self.spinCR1Phase.value()
        self._setting['CR1']['WIDTH'] = self.spinCR1Width.value()
        self._setting['CR1']['ENABLE'] = True if self.chkCR1Enable.isChecked() else False
        self._setting['CR1']['SKIP'] = 1 if self.chkCR1Skip.isChecked() else 0

        self._setting['CR2']['PHASE'] = self.spinCR2Phase.value()
        self._setting['CR2']['WIDTH'] = self.spinCR2Width.value()
        self._setting['CR2']['ENABLE'] = True if self.chkCR2Enable.isChecked() else False
        self._setting['CR2']['SKIP'] = 1 if self.chkCR2Skip.isChecked() else 0

        self._setting['CR3']['SI_PHASE'] = self.spinCR3SiPhase.value()
        self._setting['CR3']['CLK_PHASE'] = self.spinCR3ClkPhase.value()
        self._setting['CR3']['SI_WIDTH'] = self.spinCR3SiWidth.value()
        s = Settings.Settings()
        s.save()

    def device_address_changed(self, device_address):
        self._device_address = device_address

    def get_register_cr0(self):
        fall = self.spinCR0Fall.value()
        rise = self.spinCR0Rise.value()
        sample0 = self.spinCR0Sample0.value()
        sample1 = self.spinCR0Sample1.value()
        prbs = 1 if self.chkCR0PRBS.isChecked() else 0
        enable = 1 if self.chkCR0Enable.isChecked() else 0
        reg = (enable << 21) | (prbs << 20) | (sample1 << 15) | (sample0 << 10) | (rise << 5) | (fall << 0)
        return reg

    def get_register_cr1(self):
        phase = self.spinCR1Phase.value()
        width = self.spinCR1Width.value()
        enable = 1 if self.chkCR1Enable.isChecked() else 0
        skip = 1 if self.chkCR1Skip.isChecked() else 0
        reg = (skip << 11) | (enable << 10) | (width << 5) | (phase << 0)
        return reg

    def get_register_cr2(self):
        phase = self.spinCR2Phase.value()
        width = self.spinCR2Width.value()
        enable = 1 if self.chkCR2Enable.isChecked() else 0
        skip = 1 if self.chkCR2Skip.isChecked() else 0
        reg = (skip << 11) | (enable << 10) | (width << 5) | (phase << 0)
        return reg

    def get_register_cr3(self):
        si_phase = self.spinCR3SiPhase.value()
        clk_phase = self.spinCR3ClkPhase.value()
        si_width = self.spinCR3SiWidth.value()
        reg = (si_width << 13) | (clk_phase << 8) | (si_phase << 2)
        return reg
