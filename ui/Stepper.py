from PyQt5.QtCore import QRegExp, pyqtSignal
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtGui import QRegExpValidator
from designer import Ui_Stepper
from classes import Settings
from classes.Register import Register

_decay = {'slow': 0b00,
          'fast': 0b01,
          'mixed': 0b10}


class Stepper(QGroupBox, Ui_Stepper.Ui_Stepper):
    status_changed = pyqtSignal(str)

    def __init__(self, index, title, device_address):
        super(Stepper, self).__init__(title)
        self._index = index
        self._device_address = device_address
        self._base_address = 0
        self._offset = 0
        self._settings_full = None
        self._settings = None
        self._updated = False

        self.setupUi(self)
        self.setLayout(self.verticalLayout)

        self.load_settings()

        validator = QRegExpValidator(QRegExp('0x[0-9A-Fa-f]{1,8}'))
        self.editRegAddress.setValidator(validator)
        self.editRegAddress.setText('0x{:08X}'.format(self._base_address + self._offset))
        self.editName.setText(self._settings['name'])

        self.cmbStepMode.addItem('Full step (000)', userData='0b000')
        self.cmbStepMode.addItem('1/2 step (001)', userData='0b001')
        self.cmbStepMode.addItem('1/4 step (010)', userData='0b010')
        self.cmbStepMode.addItem('1/8 step (011)', userData='0b011')
        self.cmbStepMode.addItem('1/16 step (100)', userData='0b100')
        self.cmbStepMode.addItem('1/32 step (101)', userData='0b101')
        self.cmbStepMode.addItem('1/32 step (110)', userData='0b110')
        self.cmbStepMode.addItem('1/32 step (111)', userData='0b111')
        step_index = self.cmbStepMode.findData(self._settings['step'])
        if step_index > -1:
            self.cmbStepMode.setCurrentIndex(step_index)

        self.cmbDecay.addItem('Slow decay', userData='slow')
        self.cmbDecay.addItem('Fast decay', userData='fast')
        self.cmbDecay.addItem('Mixed decay', userData='mixed')
        decay_index = self.cmbDecay.findData(self._settings['decay'])
        if decay_index > -1:
            self.cmbDecay.setCurrentIndex(decay_index)

        self.chkReset.setChecked(self._settings['reset'])
        self.chkEnable.setChecked(self._settings['enable'])
        self.chkSleep.setChecked(self._settings['sleep'])

        self.btnWrite.clicked.connect(self.send)

        self.chkReset.toggled.connect(self.fields_changed)
        self.chkEnable.toggled.connect(self.fields_changed)
        self.chkSleep.toggled.connect(self.fields_changed)
        self.cmbStepMode.currentIndexChanged.connect(self.fields_changed)
        self.cmbDecay.currentIndexChanged.connect(self.fields_changed)

    def get_register_data(self):
        nRESET = int(not self.chkReset.isChecked())
        nENBL = int(not self.chkEnable.isChecked())
        nSLEEP = int(not self.chkSleep.isChecked())
        Mode = int(self.cmbStepMode.currentData(), base=2)
        decay = _decay[self.cmbDecay.currentData()]
        return (decay << 6) | (Mode << 3) | (nSLEEP << 2) | (nENBL << 1) | (nRESET << 0)

    def load_settings(self):
        s = Settings.Settings()
        self._settings_full = s.settings
        self._settings = self._settings_full['axi0']['reg595ip']['registers']['MS{}'.format(self._index)]
        self._base_address = int(self._settings_full['axi0']['reg595ip']['base_address'], base=16)
        self._offset = self._settings['offset']

    def save_settings(self):
        self._settings['reset'] = self.chkReset.isChecked()
        self._settings['enable'] = self.chkEnable.isChecked()
        self._settings['sleep'] = self.chkSleep.isChecked()
        self._settings['step'] = self.cmbStepMode.currentData()
        self._settings['decay'] = self.cmbDecay.currentData()
        s = Settings.Settings()
        s.save()

    def fields_changed(self):
        if not self._updated:
            self.btnWrite.setText(self.btnWrite.text() + '*')
            self._updated = True

    def send(self):
        writer = Register()
        registers = [{'address': self._base_address + self._offset, 'data': self.get_register_data()}]
        ret = writer.write_one(self._device_address, registers[0])
        self.status_changed.emit('Stepper {}: `{}`'.format(self._index, writer.last_error))

        if ret is False:
            return

        if self._updated:
            self.save_settings()
            self.btnWrite.setText(self.btnWrite.text()[:-1])
            self._updated = False

    def read_from_device(self):
        pass

    def device_address_changed(self, device_address):
        self._device_address = device_address
