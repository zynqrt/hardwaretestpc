from classes import Settings
import socket
import select
import argparse


class Socket:
    def __init__(self):
        self.last_error = ''

    def sock_open(self, device_address):
        sock = socket.socket()
        sock.settimeout(1.0)
        sock.connect((device_address, 8085))
        sock.setblocking(False)
        self.sock_read(sock)  # Read hello message
        return sock

    def sock_read(self, sock, timeout=0.2, size=4096):
        ready = select.select([sock], [], [], timeout)
        if ready[0]:
            data = sock.recv(size)
        else:
            data = b''
        return data

    def send(self, device_address, data):
        try:
            sock = self.sock_open(device_address)
            sock.send(data)
            status = self.sock_read(sock, size=64)
            status += self.sock_read(sock, size=64)
            self.last_error = '{}: `{}` -> {}'.format(device_address, data[:-2], status[:-2].decode('ascii'))
            sock.close()
        except Exception as e:
            self.last_error = '{}: Socket write error: {}'.format(device_address, e)
            return False, b''

        return True, status


parser = argparse.ArgumentParser(description='Задание частоты и направления')
parser.add_argument('--address', '-a', type=str, nargs='?', help='IP адрес устройства', default='192.168.88.128')
parser.add_argument('--freq', '-f', type=int, nargs='?', help='Частота шагания в тиках RTOS', default=100)
parser.add_argument('--dir', '-d', type=int, nargs='?', help='Направления: 0 или 1', default=0)

args = parser.parse_args()

Faxi = 100e6
print('Device: {}, freq: {} Hz, direction: {}'.format(args.address, args.freq, args.dir))
try:
    arr = int(Faxi / (2 * args.freq) - 1)
    Freal = Faxi / (2 * (arr + 1))
except ZeroDivisionError:
    arr = 0
    Freal = 0.0

print('ARR register: {}'.format(arr))
print('Real step frequency: {:.3f} Hz'.format(Freal))

command = 'stepper 0 {direction} {freq}\r\n'.format(direction=args.dir, freq=arr)
command = command.encode('ascii')

s = Socket()
st, ret = s.send(args.address, command)
if st:
    print(ret[:-2])
else:
    print(s.last_error)
