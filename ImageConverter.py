import sys
from PIL import Image

fin = open(sys.argv[1], 'rb')
data = fin.read()
fin.close()

print(len(data))
im = Image.frombytes(mode='I;16', size=(len(data)//2, 1), decoder_name='raw', data=data)
print(im)
im.save('tmp/im_5104.png', 'PNG')

lines = 100

im_cnv = im.convert(mode='RGB')
im_new = Image.new(mode='RGB', size=(im_cnv.size[0], im_cnv.size[1]*lines))

for i in range(lines):
    im_new.paste(im_cnv, (0, i))

im_new.save('tmp/cnv.png', 'PNG')
