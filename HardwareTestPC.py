# -*- coding: utf-8 -*-
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QApplication
from classes.Settings import Settings
from ui.MainWindow import MainWindow
import logging


if __name__ == '__main__':
    import sys
    logging.basicConfig(format='[%(name)-12s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d, %H:%M:%S', filename='logging.log')

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt='[%(name)-12s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                                  datefmt='%Y-%m-%d, %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logger = logging.getLogger('HardwareTestPC')
    logger.setLevel(logging.DEBUG)

    settings = Settings()
    settings.set_config_file('config.json')

    QCoreApplication.setOrganizationName('RPS')
    QCoreApplication.setApplicationName('Zynq Hardware Test')
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
