
all:
	pyuic5 designer/MainWindow.ui > designer/Ui_MainWindow.py
	pyuic5 designer/Stepper.ui > designer/Ui_Stepper.py
	pyuic5 designer/Discrets.ui > designer/Ui_Discrets.py
	pyuic5 designer/Register595Leds.ui > designer/Ui_Register595Leds.py
	pyuic5 designer/Register595Config.ui > designer/Ui_Register595Config.py
	pyuic5 designer/RegisterAD9826.ui > designer/Ui_RegisterAD9826.py
	pyuic5 designer/RegisterCIS.ui > designer/Ui_RegisterCIS.py
	pyuic5 designer/StepperNg.ui > designer/Ui_StepperNg.py

