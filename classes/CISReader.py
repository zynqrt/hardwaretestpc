import socket
import select
import logging
import base64


class CISReader:
    def __init__(self):
        self._logger = logging.getLogger('CISReader')
        self._logger.setLevel(logging.DEBUG)
        self.last_error = ''

    def sock_open(self, device_address):
        sock = socket.socket()
        sock.settimeout(1.0)
        sock.connect((device_address, 8085))
        sock.setblocking(False)
        self.sock_read(sock)
        return sock

    def sock_read(self, sock, timeout=0.1, size=4096):
        ready = select.select([sock], [], [], timeout)
        if ready[0]:
            data = sock.recv(size)
        else:
            data = b''
        return data

    def get_image(self, device_address, lines, color, brightness, resolution):
        if color not in ['R', 'G', 'B']:
            return None

        if resolution[0] not in [300, 600, 1200]:
            return None

        request = 'rcis {color} {brightness} {resolution} {lines}\r\n'.format(
            color=color, brightness=brightness, resolution=resolution[0], lines=lines
        )
        self._logger.debug('CIS request: `{}`'.format(request[:-2]))
        try:
            sock = self.sock_open(device_address)
            bdata = request.encode('utf-8')
            sock.send(bdata)
            status = b''
            r = self.sock_read(sock, timeout=1.0, size=4096)
            while r != b'':
                status += r
                r = self.sock_read(sock, timeout=0.2, size=4096)

            self._logger.debug('Received {} raw bytes from cis'.format(len(status)))
            sock.close()

        except Exception as e:
            self.last_error = '{}: Socket write error, error {}'.format(device_address, e)
            self._logger.error(self.last_error)
            return b''

        reply = status.split(b'$')
        if len(reply) != 2:
            return b''
        image_data = base64.b64decode(reply[0])
        status = reply[1][:-2].decode('ascii')
        self._logger.debug(f'Status: {status}')
        if status == 'ERROR':
            return b''
        return image_data
