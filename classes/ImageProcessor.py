import numpy as np
from PIL import Image
import struct
from . import Settings
import logging


class ImageProcessor:
    def __init__(self, resolution):
        if resolution not in [300, 600, 1200]:
            raise ValueError('Разрешение сканера не верное')

        self.logger = logging.getLogger('ImProccessor')
        self.logger.setLevel(logging.DEBUG)

        self._resolution = resolution
        self._settings_full = None
        self._settings = None
        self.load_settings()

    def load_settings(self):
        s = Settings.Settings()
        self._settings_full = s.settings
        for r in self._settings_full['cis']['resolutions']:
            if r['dpi'] == self._resolution:
                self._settings = r

    def bytes_to_pil(self, raw_bytes, line_count):
        pixels_active = self._settings['pixels_active']
        pixels_inactive = self._settings['pixels_inactive']

        line_size_bytes = (pixels_active + pixels_inactive) * 2 * 2  # 2 - 2 байта на отсчет, 2 - количество каналов
        if line_count * line_size_bytes != len(raw_bytes):
            return b'', b''

        lines_red = b''
        lined_green = b''
        for f in range(line_count):
            raw = raw_bytes[f * line_size_bytes: (f + 1) * line_size_bytes]
            with open(f'tmp/raw_{f:02d}.raw', 'wb') as raw_file:
                raw_file.write(raw)
            r, g = self.raw_to_channel(raw)
            lines_red += r
            lined_green += g

        im_r = self.channel_to_pil(lines_red, pixels_active + pixels_inactive, line_count)
        im_g = self.channel_to_pil(lined_green, pixels_active + pixels_inactive, line_count)
        return im_r, im_g

    def raw_to_channel(self, raw):
        pixels = self._settings['pixels_active'] + self._settings['pixels_inactive']
        red = []
        green = []
        for i in range(pixels):
            green.append(raw[i * 4])
            green.append(raw[i * 4 + 1])
            red.append(raw[i * 4 + 2])
            red.append(raw[i * 4 + 3])
        return bytes(red), bytes(green)

    def channel_to_pil(self, raw_channel, pixels_in_line, lines):
        pixels = pixels_in_line * lines
        image_data = struct.unpack('<' + pixels * 'H', raw_channel)
        image_data = np.array(image_data)
        scaled = image_data // 256
        scaled = scaled.astype('uint8')
        im = Image.frombytes(mode='L', size=(pixels_in_line, lines), decoder_name='raw', data=scaled)
        return im
