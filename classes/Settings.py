import sys
import os
import json


class Settings(object):

    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.instance = super(Settings, cls).__new__(cls)
        return cls.instance

    def set_config_file(self, file):
        root_path = os.path.dirname(sys.argv[0])
        if len(root_path) == 0:
            sep = ''
        else:
            sep = os.path.sep

        full_file_name = root_path + sep + '.'+os.path.sep + file
        if os.path.isfile(full_file_name):
            self.file = full_file_name
        else:
            self.file = None
            raise FileNotFoundError

        with open(self.file, 'r') as f:
            self.settings = json.load(f)

    def load(self):
        with open(self.file, 'r') as f:
            jdata = json.load(f)
        return jdata

    def save(self):
        with open(self.file, 'w') as f:
            json.dump(self.settings, f, indent=2)
