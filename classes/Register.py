import socket
import select
import logging


class Register:
    def __init__(self, wr_command='wr', rd_command='rr'):
        self._logger = logging.getLogger('Register')
        self._logger.setLevel(logging.DEBUG)
        self.last_error = ''
        self.wr_command = wr_command
        self.rd_command = rd_command

    def sock_open(self, device_address):
        sock = socket.socket()
        sock.settimeout(1.0)
        sock.connect((device_address, 8085))
        sock.setblocking(False)
        self.sock_read(sock)  # Read hello message
        return sock

    def sock_read(self, sock, timeout=0.2, size=4096):
        ready = select.select([sock], [], [], timeout)
        if ready[0]:
            data = sock.recv(size)
        else:
            data = b''
        return data

    def write_many(self, device_address, registers):
        try:
            self._logger.info('================== Write many: {} registers ================='.format(len(registers)))
            sock = self.sock_open(device_address)
            for r in registers:
                txdata = '{command:} 0x{address:X} 0x{data:X}\r\n'.format(command=self.wr_command,
                                                                          address=r['address'],
                                                                          data=r['data'])
                bdata = txdata.encode('utf-8')
                sock.send(bdata)
                status = self.sock_read(sock, size=64)
                status += self.sock_read(sock, size=64)
                self.last_error = '{}: `{}` -> {}'.format(device_address, txdata[:-2], status[:-2].decode('ascii'))
                self._logger.info(self.last_error)

            sock.close()

        except Exception as e:
            self.last_error = '{}: Socket write many error: {}'.format(device_address, e)
            self._logger.error(self.last_error)
            return False
        return True

    def write_one(self, device_address, register):
        try:
            sock = self.sock_open(device_address)

            txdata = '{command:} 0x{address:X} 0x{data:X}\r\n'.format(command=self.wr_command,
                                                                      address=register['address'],
                                                                      data=register['data'])
            bdata = txdata.encode('utf-8')
            sock.send(bdata)
            status = self.sock_read(sock, size=64)
            status += self.sock_read(sock, size=64)
            self.last_error = '{}: `{}` -> {}'.format(device_address, txdata[:-2], status[:-2].decode('ascii'))
            self._logger.info(self.last_error)

            sock.close()

        except Exception as e:
            self.last_error = '{}: Socket write error, error {}'.format(device_address, e)
            self._logger.error(self.last_error)
            return False

        return True

    def read_one(self, device_address, register):
        try:
            sock = self.sock_open(device_address)

            txdata = '{command:} 0x{address:X}\r\n'.format(command=self.rd_command, address=register['address'])
            btxdata = txdata.encode('utf-8')
            sock.send(btxdata)
            status = self.sock_read(sock, size=64)
            status += self.sock_read(sock, size=64)
            self.last_error = '{}: `{}` -> {}'.format(device_address, txdata[:-2], status[:-3].decode('ascii'))
            self._logger.info(self.last_error)
            sock.close()

        except Exception as e:
            self.last_error = '{}: Socket read error: {}'.format(device_address, e)
            self._logger.error(self.last_error)
            return False, 0

        status = status[:-3].decode('ascii')
        if 'OK:' in status[:3]:
            reg_data = int(status[3:], base=16)
        else:
            reg_data = 0
        return True, reg_data
