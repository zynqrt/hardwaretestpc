import socket
import select
import logging
import base64
import random
import struct
from classes.Settings import Settings

logging.basicConfig(format='[%(name)-10s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                    datefmt='%Y-%m-%d, %H:%M:%S', filename='loggingsrv.log')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt='[%(name)-10s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                              datefmt='%Y-%m-%d, %H:%M:%S')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

logger = logging.getLogger('Test Server')
logger.setLevel(logging.DEBUG)

logger.debug('Start Test Server')


def create_cis_reply(resolution, color, lines):
    s = Settings()
    s.set_config_file('config.json')
    s.load()

    valid_resolutions = []
    pixels_report = None
    for _s in s.settings['cis']['resolutions']:
        valid_resolutions.append(_s['dpi'])
        if _s['dpi'] == resolution:
            pixels_report = _s

    if resolution not in valid_resolutions:
        return b'r$ERROR'

    pixels_inactive = pixels_report['pixels_inactive']
    pixels_active = pixels_report['pixels_active']
    logger.info(f'Pixels Inactive: {pixels_inactive}')
    logger.info(f'Pixels Active: {pixels_active}')

    # Не активные пиксели, нули
    pixels_bytes = b'\x00\x00' * pixels_inactive * 2
    logger.debug('Inactive pixels bytes size: {}'.format(len(pixels_bytes)))

    for i in range(pixels_active//58):
        bp = struct.pack('H', random.randint(0, 2**16 - 1))
        pixels_bytes += bp * 58 * 2

    pixels_end = pixels_active % 58
    logger.debug('Pixels end: {}'.format(pixels_end))
    bp = struct.pack('H', random.randint(0, 2**16 - 1))
    pixels_bytes += bp * pixels_end * 2
    logger.debug('Full line size: {}'.format(len(pixels_bytes)))
    pixels_bytes *= lines
    logger.debug('Full image size ({} lines): {}'.format(lines, len(pixels_bytes)))
    r = b'$ERROR' if color == 'B' else b'$OK'
    reply_pixels = base64.b64encode(pixels_bytes) + r
    return reply_pixels


def worker(sock: socket.socket):
    welcome = "FreeRTOS command server - connection accepted.\r\nType Help to view a list of registered commands.\r\n\r\n>".encode('ascii')
    sock.send(welcome)
    cInputString = ''

    try:
        while True:
            rbyte = sock.recv(1)
            lBytes = len(rbyte)
            if lBytes > 0:
                cInChar = rbyte.decode('ascii')
                if cInChar == '\n':
                    if cInputString == 'quit':
                        lBytes = 0
                    else:
                        logger.debug('Received: {}'.format(cInputString))
                        args = list(filter(None, cInputString.split(' ')))
                        if args[0] == 'rcis' and len(args) == 5:
                            cmd = args[0]
                            color = args[1]
                            brightness = int(args[2])
                            resolution = int(args[3])
                            lines = int(args[4])
                            logger.debug('Receive comand rcis with color {}, resolution {} dpi, lines {}'.format(color, resolution, lines))
                            reply = create_cis_reply(resolution, color, lines)
                            logger.debug('Transmit {} bytes'.format(len(reply)))
                            sock.send(reply)
                        else:
                            logger.debug('Not rcis command')
                            if '0x1FF' in cInputString:  # Для теста выведем ERROR
                                sock.send(b'ERROR')
                            else:
                                sock.send(b'OK')

                        cInputString = ''
                        sock.send(b'\r\n')
                else:
                    if cInChar == '\r':
                        pass
                    elif cInChar == '\b':
                        cInputString = cInputString[:-1]
                    else:
                        cInputString += cInChar

            if lBytes == 0:
                break
    except Exception as e:
        logger.error('Ошибка сокета: {}'.format(e))
        sock.close()
        return

    logger.debug('Закрываем сокет')
    sock.close()


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('', 8085))
server_socket.listen(5)

read_list = [server_socket]
while True:
    readable, writable, errored = select.select(read_list, [], [], 1.0)
    for s in readable:
        if s is server_socket:
            client_socket, address = server_socket.accept()
            read_list.append(client_socket)
            logger.debug(f'Accepted from: {address:}')
            worker(client_socket)
            read_list.remove(client_socket)
